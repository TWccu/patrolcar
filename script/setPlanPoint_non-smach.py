#!/usr/bin/env python
import sys, getopt
import rospy
import actionlib
from geometry_msgs.msg import PointStamped
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from smachState.TopAgent import TopAgent

pointNum = 4
totalPoint = 4
counter = 0
planPoint= []

def get_clickedPoint():
    rospy.Subscriber("clicked_point", PointStamped, setPlanPoint)
    # rospy.spinonce()

def setPlanPoint(data):
    global pointNum, planPoint
    if pointNum > 0:
        x = data.point.x
        y = data.point.y
        pointNum -= 1
        point = {'x': x, 'y':y}
        planPoint.append(point)
        print "--Get clicked point--"
        print point
        print '---'
    if pointNum == 0:
        print planPoint ,'\n---'

def movebase_client(client ,point):

    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id = "map"
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose.position.x = point['x']
    goal.target_pose.pose.position.y = point['y']
    goal.target_pose.pose.orientation.w = 1.0

    client.send_goal(goal)
    wait = client.wait_for_result()
    if not wait:
        rospy.logerr("Action server not available!")
        rospy.signal_shutdown("Action server not available!")
    else:
        return client.get_result()

def initialParameter(argv):
    global pointNum
    try:
        opts, args = getopt.getopt(argv,"p:",["pointNum="])
    except getopt.GetoptError:
        print 'setPlanPoint.py -p <pointNum>'
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-p", "--pointNum"):
            pointNum = arg
            totalPoint = arg
    print 'pointNum is setted. Require ', str(pointNum), ' target point.'

if __name__ == "__main__":
    rospy.init_node('movebase_client_py', anonymous=True)
    initialParameter(sys.argv[1:])

    get_clickedPoint()
    client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
    client.wait_for_server()
    while True:
        if pointNum == 0:
            movebase_client(client, planPoint[counter % totalPoint])
            counter += 1
