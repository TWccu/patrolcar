#!/usr/bin/env python
#-*- coding:UTF-8 -*-
import sys, getopt
import rospy
# import roslib; roslib.load_manifest('devProject')
import actionlib
import smach
import smach_ros
from smachState.TopAgent import TopAgent
from smachState.SetPoint import SetPoint
from smachState.GoPoint import GoPoint

#用來管理所有參數及Method的，這樣程式碼乾淨些
topAgent = None

#讓程式執行時可設定幾個巡航點的，不太重要，可先無視(預設4個)
def initialParameter(argv):
    global topAgent
    pointNum = 4
    try:
        opts, args = getopt.getopt(argv,"p:",["pointNum="])
    except getopt.GetoptError:
        print 'setPlanPoint.py -p <pointNum>'
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-p", "--pointNum"):
            pointNum = arg
    #初始化topAgent，getInstance為Singleton的寫法，因為待會各State中都會需要這物件，所以做成單例
    topAgent = TopAgent.getInstance(pointNum)
    print 'pointNum is setted. Require ', str(topAgent.pointNum), ' target point.'

if __name__ == "__main__":
    #建立ROS的Node
    rospy.init_node('movebase_client_py', anonymous=True)
    
    #初始化topAgent
    initialParameter(sys.argv[1:])

    #建立最外層狀態機
    sm = smach.StateMachine(outcomes=['Null'])

    #打開最外層狀態機(準備加入子各個狀態)
    with sm:
        #加入輸入巡航點的狀態(設定巡航點1、巡航點2...)
        for i in range(1, topAgent.pointNum):
            target = 'SetPoint' + str(i+1)
            #若成功設定，將success導向下一個設定巡航點
            smach.StateMachine.add('SetPoint'+str(i), SetPoint(), transitions={'success': str(target)})
        #最後一個設定巡航點的狀態需接上移動至第一個巡航點，比前面幾個特別，固特別在for迴圈外設定
        smach.StateMachine.add('SetPoint'+str(topAgent.pointNum), SetPoint(), transitions={'success': 'GoPoint1'})

        #加入各個移動至巡航點的狀態(移動至巡航點1、巡航點2...)
        for i in range(1, topAgent.pointNum):
            target = 'GoPoint' + str(i+1)
            smach.StateMachine.add('GoPoint'+str(i), GoPoint(), transitions={'arrived': target})
        #最後一個移動至巡航點的狀態需接上移動至第一個巡航點，比前面幾個特別，固特別在for迴圈外設定
        smach.StateMachine.add('GoPoint'+str(topAgent.pointNum), GoPoint(), transitions={'arrived': 'GoPoint1'})

    sis = smach_ros.IntrospectionServer('server_name', sm, '/SM_ROOT')
    outcome = sm.execute(
    sis.start()

    # Wait for ctrl-c to stop the application
    rospy.spin()
    sis.stop()