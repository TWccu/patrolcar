#!/usr/bin/env python
#-*- coding:UTF-8 -*-

import rospy
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal


class TopAgent:
    '''
    pointNum = 0
    counter = 0
    planPoint = []
    '''
    __instance = None

    def __init__(self, pointNum = 4):
        if TopAgent.__instance:
            raise TopAgent.__instance
        self.pointNum = pointNum
        self.counter = 0
        self.planPoint = []
        self.client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
        TopAgent.__instance = self

    @staticmethod
    def getInstance(pointNum = 4):
        if not TopAgent.__instance:
            TopAgent.__instance = TopAgent(pointNum)
        return TopAgent.__instance

    #加入巡航點
    def insertPoint(self, point):
        self.planPoint.append(point)

    #移動至下一個巡航點
    def goNext(self):
        self.movebase_client(self.planPoint[self.counter])
        self.counter += 1
        if self.counter == self.pointNum:
            self.counter = 0       

    #移動實際的運作Method，送訊息至Topic
    def movebase_client(self,point):

        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose.position.x = point['x']
        goal.target_pose.pose.position.y = point['y']
        goal.target_pose.pose.orientation.w = 1.0

        self.client.send_goal(goal)
        #client.wait_for_result會讓程式停在這邊
        wait = self.client.wait_for_result()
        if not wait:
            rospy.loginfo("Action server not available!")
            rospy.signal_shutdown("Action server not available!")
        else:
            return client.get_result()