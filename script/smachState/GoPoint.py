#!/usr/bin/env python
#-*- coding:UTF-8 -*-

import smach
import rospy
from smachState.TopAgent import TopAgent

class GoPoint(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['arrived'])

    def execute(self, userdata):
        rospy.loginfo('Going to next point.')
        #將topAgent導進來
        topAgent = TopAgent.getInstance()
        #topAgent送出移動至下一個巡航點的訊息至Topic，這行程式碼會受client.wait_for_result影響而卡住
        #不知道client.wait_for_result的回去TopAgent裡看
        topAgent.goNext()

        return 'arrived'