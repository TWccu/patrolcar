#!/usr/bin/env python
#-*- coding:UTF-8 -*-

import smach
import rospy
from smachState.TopAgent import TopAgent
from geometry_msgs.msg import PointStamped




class SetPoint(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
        self.locked = True

    #當收到Rviz上的放置publish_point時會觸發這個Method，並將座標加入Agent的巡航點集合當中
    def callback(self,data):
        #先將topAgent的單例導進來
        topAgent = TopAgent.getInstance()
        x = data.point.x
        y = data.point.y
        point = {'x': x, 'y':y}

        #導近來後，這邊就可以加入巡航點了
        topAgent.insertPoint(point)
        self.locked = False
        self.subscriber.unregister()

    def execute(self, userdata):
        self.subscriber = rospy.Subscriber('clicked_point', PointStamped, self.callback)
        rospy.loginfo('Setting planPoint')
        #這邊會等待Rviz那邊點下巡航點
        while self.locked:
            continue
        return 'success'
